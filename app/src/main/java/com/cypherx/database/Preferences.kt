package com.cypherx.database

import android.content.Context
import android.content.SharedPreferences
import java.util.LinkedHashSet

class Preferences {


    companion object {
        private val defaultString: String? = null
        lateinit var mSharedPreferences: SharedPreferences

       // val all: Map<*, *> get() = mSharedPreferences!!.all
    }

    fun createOrOpenPreferences(mContext: Context, prefName: String) {
        mSharedPreferences = mContext.getSharedPreferences(prefName, Context.MODE_PRIVATE)
    }

    fun setValue(key: String, value: String) {
        val editor = mSharedPreferences!!.edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun getStringValue(key: String): String {
        val value = mSharedPreferences.getString(key, defaultString)
        return value ?: ""
    }


    fun getBooleanValue(key: String): Boolean {
        return mSharedPreferences!!.getBoolean(key, false)
    }

    fun setBooleanValue(key: String, value: Boolean) {
        val editor = mSharedPreferences!!.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun setHashSet(key: String, contact: LinkedHashSet<String>) {
        val editor = mSharedPreferences!!.edit()
        editor.putStringSet(key, contact)
        editor.commit()
    }

    fun getHashSet(key: String): Set<*>? {
        return mSharedPreferences!!.getStringSet(key, null)
    }

}
