package com.cypherx.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.cypherx.R
import com.cypherx.interfaces.ApiInterfaces
import com.cypherx.models.forgot.ForgotRequest
import com.cypherx.models.forgot.ForgotResponse
import com.cypherx.retrofit.APIClient
import com.cypherx.utils.CommonUtils
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_forgot_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : AppCompatActivity() {

    private var mContext: Context? = null
    private var dialog: SpotsDialog? = null
    private var strForgotMail: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        setContentView(R.layout.activity_forgot_password)
        initViews()

    }

    @SuppressLint("ResourceType")
    private fun initViews() {
        mContext = this
        dialog = SpotsDialog(mContext, resources.getString(R.string.message))
        dialog!!.setCancelable(false)

        btnForgot.setOnClickListener(clickListener)
        imgBack.setOnClickListener(clickListener)

    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btnForgot -> {
                emptyForgot()
            }
            R.id.imgBack -> {
                onBackPressed()
            }
        }
    }

    private fun emptyForgot() {
        strForgotMail = edtForgotMail!!.text.toString()

        if (!strForgotMail.isNullOrEmpty()) {
            if (!Patterns.EMAIL_ADDRESS.matcher(strForgotMail).matches()) {
                edtForgotMail!!.error = resources.getString(R.string.validMail)
                edtForgotMail!!.requestFocus()

            } else {

                if (CommonUtils.isNetworkAvailable(mContext)) {
                    forgotApi()
                } else {
                    Toast.makeText(applicationContext, R.string.network_check, Toast.LENGTH_SHORT).show()
                }

            }
        } else {
            edtForgotMail!!.error = resources.getString(R.string.email)
            edtForgotMail!!.requestFocus()
        }

    }

    private fun forgotApi() {
        dialog!!.show()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiForgot(this.buildForgot()!!)
        call.enqueue(object : Callback<ForgotResponse> {
            override fun onFailure(call: Call<ForgotResponse>?, t: Throwable?) {
                dialog!!.dismiss()

                Toast.makeText(applicationContext, t!!.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<ForgotResponse>?, response: Response<ForgotResponse>?) {
                dialog!!.dismiss()
                if (response != null)
                    if (response.body() != null) {
                        if (response.body()!!.status == "Success") {
                            Log.d("hghg", response.body().toString())
                            Toast.makeText(applicationContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                            onBackPressed()
                        } else {
                            Toast.makeText(applicationContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }


                    } else {
                        CommonUtils.showToast(applicationContext, "Email invalid")
                    }
            }
        })
    }

    private fun buildForgot(): ForgotRequest? {

        val forgotRequest = ForgotRequest()
        forgotRequest.emailId = strForgotMail
        return forgotRequest

    }

    override fun onBackPressed() {
        super.onBackPressed()

        val intent = Intent(this, LoginRegisterActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
        finish()
    }
}


