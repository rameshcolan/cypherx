package com.cypherx.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.cypherx.R
import com.cypherx.database.Preferences
import com.cypherx.interfaces.ApiInterfaces
import com.cypherx.models.login.LoginRequest
import com.cypherx.models.login.LoginResponse
import com.cypherx.models.login.LoginResponseItem
import com.cypherx.models.signup.SignUpRequest
import com.cypherx.models.signup.SignUpResponse
import com.cypherx.retrofit.APIClient
import com.cypherx.utils.CommonUtils
import com.cypherx.utils.Constants
import com.cypherx.utils.Constants.SERVER_ERROR
import com.cypherx.utils.Constants.SERVICE_SUCCESS
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_login_resigter.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginRegisterActivity : AppCompatActivity() {

    private var mContext: Context? = null
    private var dialog: SpotsDialog? = null
    private var strLoginEmail: String? = null
    private var strLoginPsw: String? = null
    private var strRegMail: String? = null
    private var strRegReference: String? = null
    private var srtRegPswd: String? = null
    private var strRegConfirm: String? = null
    private var strRegUsername: String? = null
    private var strRegEth: String? = null
    private var strRegBtc: String? = null
    private var pre: Preferences? = null
    private var responseItems = LoginResponseItem()
    internal var etherWalletAddress: String? = ""
    internal var bitcoinWalletReceivingAddress: String? = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_resigter)
        initViews()

    }

    @SuppressLint("SetTextI18n", "ResourceType")
    private fun initViews() {
        pre = Preferences()
        mContext = this
        relLog.visibility = View.VISIBLE
        relReg.visibility = View.GONE
        txt_login.setBackgroundResource(R.drawable.login_bg)
        txt_register.setBackgroundResource(0)

//        edtLoginEmail.setText("admin@gmail.com")
//        edtLoginpassword.setText("Mathan@123")

          edtLoginEmail.setText("gwtm.srsh@gmail.com")
          edtLoginpassword.setText("Test@123")

        // edtLoginEmail.setText("ramesh.k@colanonline.com")
        //  edtLoginpassword.setText("Test@123")


        dialog = SpotsDialog(mContext, resources.getString(R.string.message))
        dialog!!.setCancelable(false)
        relLog.setOnClickListener(clickListener)
        relReg.setOnClickListener(clickListener)
        btn_login.setOnClickListener(clickListener)
        btn_register.setOnClickListener(clickListener)
        txt_register.setOnClickListener(clickListener)
        txt_login.setOnClickListener(clickListener)
        txt_forgot.setOnClickListener(clickListener)

    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btn_login -> {
                emptyLoginCheck()
//                val intent = Intent(applicationContext, DashboardActivity::class.java)
//                startActivity(intent)
//                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
            R.id.btn_register -> {
                emptycheckregister()
            }
            R.id.txt_register -> {
                callRegister()
            }

            R.id.txt_forgot -> {
                callForgot()
            }
            R.id.txt_login -> {
                callLogin()
            }
        }
    }


    private fun emptycheckregister() {

        strRegUsername = edtRegUserName!!.text.toString()
        strRegMail = edtRegEmail!!.text.toString()
        strRegReference = edtRegReference!!.text.toString()
        srtRegPswd = edtRegPwd!!.text.toString()
        strRegConfirm = edtRegConfirmPwd!!.text.toString()
        strRegEth = edtRegEth!!.text.toString()
        strRegBtc = edtRegBtc!!.text.toString()

        if (strRegUsername.isNullOrEmpty()) {
            edtRegUserName!!.error = resources.getString(R.string.username)
            edtRegUserName!!.requestFocus()
        } else if (!strRegMail.isNullOrEmpty()) {
            if (!Patterns.EMAIL_ADDRESS.matcher(strRegMail).matches()) {
                edtRegEmail!!.error = resources.getString(R.string.validMail)
                edtRegEmail!!.requestFocus()
            } else if (srtRegPswd.isNullOrEmpty()) {
                edtRegPwd!!.error = resources.getString(R.string.password)
                edtRegPwd!!.requestFocus()
            } else if (strRegConfirm.isNullOrEmpty()) {
                edtRegConfirmPwd!!.error = resources.getString(R.string.confirmPassword)
                edtRegConfirmPwd!!.requestFocus()
            } else if (srtRegPswd!! != strRegConfirm) {
                edtRegConfirmPwd!!.error = resources.getString(R.string.password_validation)
            } else if (strRegEth.isNullOrEmpty()) {
                edtRegEth!!.error = resources.getString(R.string.eth_password)
                edtRegPwd!!.requestFocus()
            } else if (strRegBtc.isNullOrEmpty()) {
                edtRegBtc!!.error = resources.getString(R.string.btc_password)
                edtRegBtc!!.requestFocus()
            } else {
                if (CommonUtils.isNetworkAvailable(mContext)) {
                    callRegisterApi()
                } else {
                    Toast.makeText(applicationContext, R.string.network_check, Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            edtRegEmail!!.error = resources.getString(R.string.email)
            edtRegEmail!!.requestFocus()
        }
    }

    private fun callRegisterApi() {
        dialog!!.show()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiSignUp(this.buildSignUp()!!)
        call.enqueue(object : Callback<SignUpResponse> {
            override fun onResponse(call: Call<SignUpResponse>?, response: Response<SignUpResponse>?) {
                dialog!!.dismiss()
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == SERVICE_SUCCESS) {
                            callLogin()
                            Toast.makeText(applicationContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                            pre?.setValue(Constants.BTC, strRegBtc.toString())
                            pre?.setValue(Constants.ETH, strRegEth.toString())
                        } else {
                            Toast.makeText(applicationContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                      //  Toast.makeText(applicationContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()

                    }
                }else {
                    Toast.makeText(applicationContext, "" + response!!.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<SignUpResponse>?, t: Throwable?) {
                dialog!!.dismiss()
                Toast.makeText(mContext, t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun buildSignUp(): SignUpRequest? {

        val signUpRequest = SignUpRequest()
        signUpRequest.userName = strRegUsername
        signUpRequest.emailId = strRegMail

        if (strRegReference.isNullOrEmpty()) {
            signUpRequest.referenceId = "1"
        } else {
            signUpRequest.referenceId = strRegReference
        }
        signUpRequest.password = srtRegPswd
        signUpRequest.confirmPassword = strRegConfirm
        signUpRequest.etherWalletPassword = strRegEth
        signUpRequest.bitcoinWalletPassword = strRegBtc

        return signUpRequest
    }


    private fun emptyLoginCheck() {

        strLoginEmail = edtLoginEmail.text.toString().trim()
        strLoginPsw = edtLoginpassword.text.toString().trim()

        if (!strLoginEmail.isNullOrEmpty()) {
            if (!Patterns.EMAIL_ADDRESS.matcher(strLoginEmail).matches()) {
                edtLoginEmail!!.error = resources.getString(R.string.validMail)

            } else if (TextUtils.isEmpty(edtLoginpassword!!.text.toString().trim())) {

                edtLoginEmail!!.error = null
                edtLoginpassword!!.error = resources.getString(R.string.correct_password)
                edtLoginpassword!!.requestFocus()

            } else {

                if (CommonUtils.isNetworkAvailable(mContext)) {
                    loginApi()
                } else {
                    Toast.makeText(applicationContext, R.string.network_check, Toast.LENGTH_SHORT).show()
                }


            }
        } else {
            edtLoginEmail!!.error = resources.getString(R.string.email)
            edtLoginEmail!!.requestFocus()
        }
    }

    private fun loginApi() {
        dialog!!.show()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiLogin(this.buildLogin()!!)
        call.enqueue(object : Callback<LoginResponse> {

            override fun onResponse(call: Call<LoginResponse>?, loginResponse: Response<LoginResponse>?) {

                dialog!!.dismiss()
                if (loginResponse != null) {
                    if (loginResponse.body() != null) {
                        if (loginResponse.body()!!.status == Constants.SERVICE_SUCCESS) {

                            pre?.setBooleanValue("isLogged", Constants.IS_LOGGED_IN)
                            responseItems = loginResponse.body()!!.loginInfo!!
                            etherWalletAddress = responseItems.etherWalletAddress
                            bitcoinWalletReceivingAddress = responseItems.bitcoinWalletReceivingAddress

                            Toast.makeText(applicationContext, "" + loginResponse.body()!!.message, Toast.LENGTH_SHORT).show()
                            pre?.setValue(Constants.SESSION, "" + loginResponse.body()!!.loginInfo!!.sessionId)
                            pre?.setValue(Constants.BTC, "" + loginResponse.body()!!.loginInfo!!.bitcoinWalletReceivingAddress)
                            pre?.setValue(Constants.ETH, "" + loginResponse.body()!!.loginInfo!!.etherWalletAddress)
                            pre?.setValue(Constants.USER, "" + loginResponse.body()!!.loginInfo!!.userName)
                            pre?.setValue(Constants.EMAIL, "" + loginResponse.body()!!.loginInfo!!.emailId)
                            callDashboard(loginResponse.body()!!.loginInfo!!.sessionId, loginResponse.body()!!.loginInfo!!.roleId)
                        } else {
                            Toast.makeText(applicationContext, "" + loginResponse.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(applicationContext, "" + loginResponse.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(applicationContext,SERVER_ERROR,Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                Toast.makeText(applicationContext, "" + "", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun callDashboard(sessionId: String?, roleId: Int?) {
        val i = Intent(applicationContext, DashboardActivity::class.java)
        i.putExtra(Constants.SESSION, sessionId)
        i.putExtra(Constants.ROLEID, roleId)
        startActivity(i)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    private fun buildLogin(): LoginRequest? {
        val loginRequest = LoginRequest()
        loginRequest.emailId = strLoginEmail
        loginRequest.password = strLoginPsw
        return loginRequest

    }

    private fun callLogin() {
        relLog.visibility = View.VISIBLE
        relReg.visibility = View.GONE
        txt_login.setBackgroundResource(R.drawable.login_bg)
        txt_register.setBackgroundResource(0)

    }

    private fun callForgot() {
        val i = Intent(applicationContext, ForgotPasswordActivity::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    private fun callRegister() {
        relLog.visibility = View.GONE
        relReg.visibility = View.VISIBLE
        txt_login.setBackgroundResource(0)
        txt_register.setBackgroundResource(R.drawable.login_bg)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}



