package com.cypherx.activities

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.cypherx.R
import com.cypherx.fragments.BuyMoreTokesFragment
import com.cypherx.fragments.HistoryFragment
import com.cypherx.fragments.LiveCryptoPricingFragment
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.app_bar_dashboard.*
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.cypherx.adapter.CustomPageAdapterNew
import com.cypherx.database.Preferences
import com.cypherx.fragments.DashboardFragment
import com.cypherx.interfaces.ApiInterfaces
import com.cypherx.models.Cyphercoin.CypherCoinRequest
import com.cypherx.models.Cyphercoin.CypherCoinResponse
import com.cypherx.models.DataObject
import com.cypherx.models.bitcoinbalance.BitcoinBalanceRequest
import com.cypherx.models.bitcoinbalance.BitcoinBalanceResponse
import com.cypherx.models.etherCoinBalance.EtherCoinRequest
import com.cypherx.models.etherCoinBalance.EtherCoinResponse
import com.cypherx.models.login.LoginResponse
import com.cypherx.models.logout.LogoutRequest
import com.cypherx.models.logout.LogoutResponse
import com.cypherx.models.signup.SignUpRequest
import com.cypherx.models.signup.SignUpResponse
import com.cypherx.retrofit.APIClient
import com.cypherx.utils.CommonUtils
import com.cypherx.utils.Constants
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.content_dashboard.*
import kotlinx.android.synthetic.main.nav_header_dashboard.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DashboardActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var mContext: Context? = null

    private var dialog: SpotsDialog? = null
    var pre: Preferences? = null
    var frag: Fragment? = null
    var id: String? = null
    var role: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        setContentView(R.layout.activity_dashboard)

        initViews()

        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        toolbar.setNavigationIcon(R.drawable.nav_icon)
        toolbar_title.text = resources.getString(R.string.dashboard)

        val navHeaderView = nav_view.inflateHeaderView(R.layout.nav_header_dashboard)
        val txt_user_name = navHeaderView.findViewById(R.id.txt_user_name) as TextView
        val txtUserEmail = navHeaderView.findViewById(R.id.txtUserEmail) as TextView

        val name = pre?.getStringValue(Constants.USER)
        val email = pre?.getStringValue(Constants.EMAIL)
        if (!name.isNullOrEmpty() and !email.isNullOrEmpty()) {
            txt_user_name.text = name
            txtUserEmail.text = email
        } else {
            txt_user_name.text = "User"
            txtUserEmail.text = "user@gmail.com"
        }

        nav_view.setNavigationItemSelectedListener(this)
    }


    @SuppressLint("ResourceType")
    private fun initViews() {
        mContext = this
        pre = Preferences()

        val intent = intent
        id = intent.getStringExtra(Constants.SESSION)
        role = intent.getIntExtra(Constants.ROLEID, 0)

        dialog = SpotsDialog(mContext, resources.getString(R.string.message))
        dialog!!.setCancelable(false)

        if (role == 0) {
            hideItem()
        }

        val fragmentManager = supportFragmentManager
        val ft = fragmentManager.beginTransaction()
        ft.replace(R.id.frame, DashboardFragment())
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        ft.addToBackStack(null)
        ft.commit()
    }

    @SuppressLint("ResourceType")
    private fun hideItem() {
        val nav_Menu = nav_view.menu
        nav_Menu.findItem(R.id.nav_buy_more_tokens).isVisible = false
    }

    override fun onBackPressed() {
        /*
         if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
             drawer_layout.closeDrawer(GravityCompat.START)
         } else {
             super.onBackPressed()
         }*/
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {


        when (item.itemId) {

            R.id.nav_dashboard -> {
                toolbar_title.text = resources.getString(R.string.dashboard)
                frag = DashboardFragment()
            }

            R.id.nav_buy_more_tokens -> {
                toolbar_title.text = resources.getString(R.string.buyMoreTokens)
                frag = BuyMoreTokesFragment()
            }

            R.id.nav_history -> {
                toolbar_title.text = resources.getString(R.string.history)
                frag = HistoryFragment()
            }

            R.id.nav_live_crypto_pricing -> {
                toolbar_title.text = resources.getString(R.string.txt_live_crypto_pricing)
                frag = LiveCryptoPricingFragment()
            }

            R.id.nav_logout -> {

                if (CommonUtils.isNetworkAvailable(mContext)) {
                    logoutAlert()
                } else {
                    Toast.makeText(applicationContext, R.string.network_check, Toast.LENGTH_SHORT).show()
                }
            }
        }

        if (frag != null) {
            val fragmentManager = supportFragmentManager
            val ft = fragmentManager.beginTransaction()
            ft.replace(R.id.frame, frag)
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            ft.addToBackStack(null)
            ft.commit()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun logoutAlert() {
        val builder = AlertDialog.Builder(this@DashboardActivity)
        builder.setTitle(R.string.title)
        builder.setMessage(R.string.txt_logout_title)

        builder.setPositiveButton(R.string.yes) { dialog, _ ->

            if (CommonUtils.isNetworkAvailable(mContext)) {
                dialog.dismiss()
                callLogoutApi()
            } else {
                Toast.makeText(applicationContext, R.string.network_check, Toast.LENGTH_SHORT).show()
            }
        }

        builder.setNegativeButton(R.string.no) { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    private fun callLogoutApi() {
        dialog!!.show()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiLogout(buildLogout()!!)
        call.enqueue(object : Callback<LogoutResponse> {
            override fun onResponse(call: Call<LogoutResponse>?, response: Response<LogoutResponse>?) {
                dialog!!.dismiss()
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            callLogout()
                            Toast.makeText(applicationContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        } else {
                            // Toast.makeText(applicationContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                            val msg = response.body()!!.message
                            callLogin(msg)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<LogoutResponse>?, t: Throwable?) {
                dialog!!.dismiss()
                Toast.makeText(mContext, t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun callLogin(msg: String?) {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setMessage("$msg!")
        alertDialog.setButton("OK", DialogInterface.OnClickListener { dialog, _ ->
            dialog.dismiss()
            callLogout()
        }
        )
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

    private fun callLogout() {
        val intent = Intent(applicationContext, LoginRegisterActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
        finish()
    }


    private fun buildLogout(): LogoutRequest? {
        val logoutRequest = LogoutRequest()
        //  val s = pre?.getStringValue(Constants.SERVICE_SESSION_ID)
        logoutRequest.sessionId = id
        return logoutRequest
    }
}

