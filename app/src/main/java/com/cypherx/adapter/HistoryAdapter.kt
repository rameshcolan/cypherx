package com.cypherx.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.cypherx.R
import com.cypherx.models.history.HistoryResponse


class HistoryAdapter(var mContext: Context?, var historyData: ArrayList<HistoryResponse.TransactionHistory>) :
        RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.activity_history_adapter, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return historyData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.txtBonus.text = historyData[position].transferedAmount
        holder.txtBonusWallet.text = historyData[position].fromAddress

        var cyx = historyData[position].transferMode

        if (cyx.equals("CYX")) {
            holder.imgBonusCoin.setImageResource(R.drawable.ic_cyx)
        } else if (cyx.equals("BTC")) {
            holder.imgBonusCoin.setImageResource(R.drawable.ic_bitcoin)
        }else{
            holder.imgBonusCoin.setImageResource(R.drawable.ic_eth_dashboard)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val imgBonusCoin = itemView.findViewById(R.id.img_coin_bit) as ImageView
        val txtBonus = itemView.findViewById(R.id.txt_bonus) as TextView
        val txtBonusWallet = itemView.findViewById(R.id.txt_bonus_add) as TextView

    }

}
