package com.cypherx.adapter

import android.annotation.SuppressLint
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.cypherx.R

class LiveCryptoAdapter(var activity: FragmentActivity?, var priceList: ArrayList<String>?) : RecyclerView.Adapter<LiveCryptoAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiveCryptoAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_live_crypto_pricing, parent, false)
        return ViewHolder(v)
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: LiveCryptoAdapter.ViewHolder, position: Int) {

        holder.textRate.text =  "$"+" " +priceList!![position]

        if (position == 0){
            holder.imgCoin.setImageResource(R.drawable.ic_cyx)
            holder.txtCoinName.text = activity!!.getString(R.string.cyx)
            holder.txtCoins.text = activity!!.getString(R.string.cypher)
        }else if (position == 1) {
            holder.imgCoin.setImageResource(R.drawable.ic_bitcoin)
            holder.txtCoinName.text = activity!!.getString(R.string.btc)
            holder.txtCoins.text = activity!!.getString(R.string.bitcoin)
        } else if (position == 2) {
            holder.imgCoin.setImageResource(R.drawable.ic_eth_dashboard)
            holder.txtCoins.text = activity!!.getString(R.string.Etherium)
            holder.txtCoinName.text = activity!!.getString(R.string.eth)
        }
    }

    override fun getItemCount(): Int {
        return priceList!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textRate = itemView.findViewById(R.id.txt_rate) as TextView
        val imgCoin = itemView.findViewById(R.id.img_bitcoins) as ImageView
        val txtCoinName = itemView.findViewById(R.id.txt_coinname) as TextView
        val txtCoins = itemView.findViewById(R.id.txt_coins) as TextView
    }
}

