package com.cypherx.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cypherx.R;
import com.cypherx.models.DataObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class CustomPageAdapterNew extends PagerAdapter {

    private Context context;
    private List<DataObject> dataObjectList;
    private LayoutInflater layoutInflater;
    private String balance = "";


    public CustomPageAdapterNew(Context context, List<DataObject> dataObjectList) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
        this.dataObjectList = dataObjectList;
    }

    @Override
    public int getCount() {
        return dataObjectList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = this.layoutInflater.inflate(R.layout.activity_wallet, container, false);

        ImageView displayImage = view.findViewById(R.id.imgWallet);
        RelativeLayout rlBackground = view.findViewById(R.id.rlBackground);
        TextView coinBal = view.findViewById(R.id.txtwallet);
        TextView coinChanges = view.findViewById(R.id.txtRates);

        displayImage.setImageResource(this.dataObjectList.get(position).getImageId());
        rlBackground.setBackgroundResource(this.dataObjectList.get(position).getImageName());
        balance = this.dataObjectList.get(position).getImgCoinBalance();
        coinChanges.setText(this.dataObjectList.get(position).getTxtCoinText());
        coinBal.setText(balance);

        container.addView(view);
        return view;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
