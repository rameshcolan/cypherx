package com.cypherx.interfaces

import com.cypherx.models.Cyphercoin.CypherCoinRequest
import com.cypherx.models.Cyphercoin.CypherCoinResponse
import com.cypherx.models.bitcoinbalance.BitcoinBalanceRequest
import com.cypherx.models.bitcoinbalance.BitcoinBalanceResponse
import com.cypherx.models.bonus.BonusRequest
import com.cypherx.models.bonus.BonusResponse
import com.cypherx.models.contributeCrowdSale.ContributeCrowdSaleResponse
import com.cypherx.models.contributeCrowdSale.EtherBuyRequest
import com.cypherx.models.etherCoinBalance.EtherCoinRequest
import com.cypherx.models.etherCoinBalance.EtherCoinResponse
import com.cypherx.models.forgot.ForgotRequest
import com.cypherx.models.forgot.ForgotResponse
import com.cypherx.models.history.HistoryRequest
import com.cypherx.models.history.HistoryResponse
import com.cypherx.models.liveCryptoPrice.LiveCryptoPriceResponse
import com.cypherx.models.login.LoginRequest
import com.cypherx.models.login.LoginResponse
import com.cypherx.models.logout.LogoutRequest
import com.cypherx.models.logout.LogoutResponse
import com.cypherx.models.signup.SignUpRequest
import com.cypherx.models.signup.SignUpResponse
import com.cypherx.models.startdate.DateRequest
import com.cypherx.models.startdate.DateResponse
import com.cypherx.models.tokenrate.TokenRateRequest
import com.cypherx.models.tokenrate.TokenRateResponse
import com.cypherx.utils.Constants.SERVICE_BITCOIN_BALANCE
import com.cypherx.utils.Constants.SERVICE_BONUS
import com.cypherx.utils.Constants.SERVICE_CONTRIBUTE_CROWDSALE
import com.cypherx.utils.Constants.SERVICE_CYPHERX_BALANCE
import com.cypherx.utils.Constants.SERVICE_DATE
import com.cypherx.utils.Constants.SERVICE_ETHER_BALANCE
import com.cypherx.utils.Constants.SERVICE_FORGOT
import com.cypherx.utils.Constants.SERVICE_HISTORY
import com.cypherx.utils.Constants.SERVICE_LIVE_CRYPTO_PRICE
import com.cypherx.utils.Constants.SERVICE_LOGIN
import com.cypherx.utils.Constants.SERVICE_LOGOUT
import com.cypherx.utils.Constants.SERVICE_SIGNUP
import com.cypherx.utils.Constants.SERVICE_TOKEN
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiInterfaces {

    @POST(SERVICE_LOGIN)
    fun apiLogin(@Body loginRequest: LoginRequest): Call<LoginResponse>

    @POST(SERVICE_SIGNUP)
    fun apiSignUp(@Body signUpRequest: SignUpRequest): Call<SignUpResponse>

    @POST(SERVICE_HISTORY)
    fun apiHistory(@Body historyRequest: HistoryRequest): Call<HistoryResponse>

    @POST(SERVICE_FORGOT)
    fun apiForgot(@Body forgotRequest: ForgotRequest): Call<ForgotResponse>

    @POST(SERVICE_LOGOUT)
    fun apiLogout(@Body logoutRequest: LogoutRequest): Call<LogoutResponse>

    @POST(SERVICE_BITCOIN_BALANCE)
    fun apiBitCoinBalance(@Body bitCoinBalanceRequest: BitcoinBalanceRequest): Call<BitcoinBalanceResponse>

    @POST(SERVICE_ETHER_BALANCE)
    fun apiEtherBalace(@Body etherCoinBalanceRequest: EtherCoinRequest): Call<EtherCoinResponse>

    @POST(SERVICE_CYPHERX_BALANCE)
    fun apiCypherBalance(@Body cypherCoinRequest: CypherCoinRequest): Call<CypherCoinResponse>

    @POST(SERVICE_CONTRIBUTE_CROWDSALE)
    fun apiContribute(@Body etherBuyRequest: EtherBuyRequest): Call<ContributeCrowdSaleResponse>

    @POST(SERVICE_BONUS)
    fun apiBonus(@Body bonusRequest: BonusRequest): Call<BonusResponse>

    @GET(SERVICE_LIVE_CRYPTO_PRICE)
    fun apiLiveCurrency(): Call<LiveCryptoPriceResponse>

    @POST(SERVICE_TOKEN)
    fun apiToken(@Body tokenRate: TokenRateRequest): Call<TokenRateResponse>


    @POST(SERVICE_DATE)
    fun apiDate(@Body dateRequest: DateRequest): Call<DateResponse>
}