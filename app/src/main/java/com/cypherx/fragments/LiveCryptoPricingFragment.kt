package com.cypherx.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import com.cypherx.R
import com.cypherx.adapter.LiveCryptoAdapter
import com.cypherx.interfaces.ApiInterfaces
import com.cypherx.models.liveCryptoPrice.LiveCryptoPriceResponse
import com.cypherx.models.signup.SignUpResponse
import com.cypherx.models.tokenrate.TokenRateRequest
import com.cypherx.models.tokenrate.TokenRateResponse
import com.cypherx.retrofit.APIClient
import com.cypherx.utils.CommonUtils
import com.cypherx.utils.Constants
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.app_bar_dashboard.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LiveCryptoPricingFragment : Fragment() {

    private var dialog: SpotsDialog? = null
    var liveRecycler: RecyclerView? = null
    var mContext: Context? = null
    var btcLivePrice: String? = null
    var ethLivePrice: String? = null
    var priceList: ArrayList<String>? = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_live_crpto_pricing, container, false)
        initViews(v)
        return v
    }

    @SuppressLint("ResourceType")
    private fun initViews(view: View) {
        dialog = SpotsDialog(activity, resources.getString(R.string.message))
        dialog!!.setCancelable(false)


        if (CommonUtils.isNetworkAvailable(activity)) {
            liveCryptoPriceListApi()
            cyxLiveRate()

        } else {
            Toast.makeText(activity, R.string.network_check, Toast.LENGTH_SHORT).show()
        }


        liveRecycler = view.findViewById(R.id.view_price) as RecyclerView
        val noRecored = view.findViewById(R.id.noRecored) as TextView
        liveRecycler!!.layoutManager = LinearLayoutManager(mContext)
    }

    private fun cyxLiveRate() {
        dialog!!.show()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiToken(buildTokenRequest())
        call.enqueue(object : Callback<TokenRateResponse> {
            override fun onResponse(call: Call<TokenRateResponse>?, response: Response<TokenRateResponse>?) {
                dialog!!.dismiss()
                if (response != null && response.isSuccessful) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            val cyxRate = response.body()!!.tokenRate.toString()
                            priceList!!.add(cyxRate)
                            // Toast.makeText(activity, "" + Constants.SERVICE_SUCCESS, Toast.LENGTH_SHORT).show()
                        } else {

                            Toast.makeText(activity, "" + Constants.SERVICE_SUCCESS, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<TokenRateResponse>?, t: Throwable?) {
                dialog!!.dismiss()
                Toast.makeText(activity, t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun liveCryptoPriceListApi() {
        dialog!!.show()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiLiveCurrency()
        call.enqueue(object : Callback<LiveCryptoPriceResponse> {
            override fun onResponse(call: Call<LiveCryptoPriceResponse>?, response: Response<LiveCryptoPriceResponse>?) {
                dialog!!.dismiss()
                if (response != null) {
                    if (response.body() != null) {
                        btcLivePrice = response.body()!!.btc!!.usd.toString()
                        ethLivePrice = response.body()!!.eth!!.usd.toString()
                        priceList!!.add(btcLivePrice!!)
                        priceList!!.add(ethLivePrice!!)
                        liveRecycler!!.adapter = LiveCryptoAdapter(activity, priceList)
                        liveRecycler!!.setHasFixedSize(true)
                    }
                }
            }

            override fun onFailure(call: Call<LiveCryptoPriceResponse>?, t: Throwable?) {
                dialog!!.dismiss()
                CommonUtils.showToast(activity!!, "" + t?.message)
            }
        })
    }

    fun buildTokenRequest(): TokenRateRequest {

        var buildTokenRequest = TokenRateRequest()
        return buildTokenRequest
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).toolbar_title.text = resources.getString(R.string.cryptoPricing)
    }

    override fun onDetach() {
        super.onDetach()
        (activity as AppCompatActivity).toolbar_title.text = resources.getString(R.string.dashboard)
    }
}
