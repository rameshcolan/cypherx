package com.cypherx.fragments


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.cypherx.R
import com.cypherx.adapter.CustomPageAdapterNew
import com.cypherx.models.DataObject
import kotlinx.android.synthetic.main.app_bar_dashboard.*
import android.widget.TextView
import com.cypherx.database.Preferences
import com.cypherx.interfaces.ApiInterfaces
import com.cypherx.models.contributeCrowdSale.ContributeCrowdSaleResponse
import com.cypherx.models.contributeCrowdSale.EtherBuyRequest
import com.cypherx.retrofit.APIClient
import com.cypherx.utils.CommonUtils
import com.cypherx.utils.Constants
import dmax.dialog.SpotsDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BuyMoreTokesFragment : Fragment(), AdapterView.OnItemSelectedListener {

    var viewPager: ViewPager? = null
    private var mCustomPagerAdapter: CustomPageAdapterNew? = null
    var btnBuy: Button? = null
    private var dialog: SpotsDialog? = null
    var spinnerItems = arrayOf("PAY", "BTC", "ETH")
    var title: String? = ""
    var spinCoin: Spinner? = null
    var pre: Preferences? = null
    var btcWallAdd: String? = null
    var ethWallAdd: String? = null
    var token: EditText? = null
    var edtAmount: EditText? = null
    var amt: String? = null
    private var b: String? = null
    private var c: String? = null
    private var e: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_buy_more_tokes, container, false)
        initViews(v)
        return v
    }

    @SuppressLint("ResourceType")
    private fun initViews(v: View?) {
        pre = Preferences()
        dialog = SpotsDialog(activity, resources.getString(R.string.message))
        dialog!!.setCancelable(false)
        getDataFromPre()

        viewPager = v!!.findViewById(R.id.fragment_main_view_pager) as ViewPager
        btnBuy = v.findViewById(R.id.btnBuy) as Button
        spinCoin = v.findViewById(R.id.spinCoin) as Spinner
        token = v.findViewById(R.id.edtToken) as EditText
        edtAmount = v.findViewById(R.id.edtAmount) as EditText

        val getData = dataSource()
        mCustomPagerAdapter = CustomPageAdapterNew(activity, getData)
        viewPager!!.pageMargin = 80
        viewPager!!.offscreenPageLimit = 30
        viewPager!!.adapter = mCustomPagerAdapter

        val aa = ArrayAdapter(activity, R.layout.simple_spinner, spinnerItems)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinCoin!!.adapter = aa


        btnBuy?.setOnClickListener(clickListener)
        spinCoin?.onItemSelectedListener = this

    }

    private fun getDataFromPre() {
        btcWallAdd = pre?.getStringValue(Constants.BTC)
        ethWallAdd = pre?.getStringValue(Constants.ETH)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (position == 1) {
            title = resources.getString(R.string.bitcoin)
            token!!.setText(btcWallAdd).toString()
        } else if (position == 2) {
            title = resources.getString(R.string.etheriun)
            token!!.setText(ethWallAdd).toString()
        } else {
            token!!.setText("").toString()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.btnBuy -> {
                amt = edtAmount!!.text.toString()
                val pos = spinCoin?.selectedItemPosition
                if (!amt.isNullOrEmpty()) {
                    if ((pos == 1) or (pos == 2)) {
                        alert()
                    } else {
                        Constants.alertDialogShow(activity!!, resources.getString(R.string.payment_type))
                    }
                } else {
                    Constants.alertDialogShow(activity!!, resources.getString(R.string.enter_amount))
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun alert() {
        val dialogView = LayoutInflater.from(activity).inflate(R.layout.popup_buy_more_token, null)
        val ethPa = dialogView.findViewById(R.id.edtEBPassoword) as EditText
        val txtTitle = dialogView.findViewById(R.id.txtTitle) as TextView
        val btnSubmit = dialogView.findViewById(R.id.btnSubmit) as Button

        txtTitle.text = "Enter".plus(" $title ").plus("Password")

        val builder = AlertDialog.Builder(activity).setView(dialogView)

        val dialog = builder.show()
        btnSubmit.setOnClickListener {
            val pass = ethPa.text.toString()

            if (CommonUtils.isNetworkAvailable(activity)) {
                callEbPassword(pass)
                dialog.dismiss()
            } else {
                Toast.makeText(activity, R.string.network_check, Toast.LENGTH_SHORT).show()
            }
        }
        dialog.setCanceledOnTouchOutside(true)
    }

    private fun callEbPassword(pass: String) {
        dialog!!.show()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiContribute(tokenBuy(pass)!!)

        call.enqueue(object : Callback<ContributeCrowdSaleResponse> {
            override fun onResponse(call: Call<ContributeCrowdSaleResponse>?,
                                    response: Response<ContributeCrowdSaleResponse>?) {
                dialog!!.dismiss()
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            Toast.makeText(activity, "" + response.body()!!.message,
                                    Toast.LENGTH_SHORT).show()
                           /* val getData = dataSource()

                            mCustomPagerAdapter = CustomPageAdapterNew(activity, getData)
                            viewPager!!.adapter = mCustomPagerAdapter*/
                            mCustomPagerAdapter!!.notifyDataSetChanged()


                        } else {
                            Toast.makeText(activity, "" + response.body()!!.message,
                                    Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<ContributeCrowdSaleResponse>?, t: Throwable?) {
                dialog!!.dismiss()
                Toast.makeText(activity, t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun tokenBuy(pass: String): EtherBuyRequest? {

        val crowdRequest = EtherBuyRequest()
        crowdRequest.sessionId = pre?.getStringValue(Constants.SESSION)
        crowdRequest.requestTokens = amt!!.toDouble()
        if (spinCoin?.selectedItemPosition == 1) {
            crowdRequest.bitcoinWalletPassword = pass
            crowdRequest.selectTransactionType = 0
        } else if (spinCoin?.selectedItemPosition == 2) {
            crowdRequest.etherWalletPassword = pass
            crowdRequest.selectTransactionType = 1
        }
        return crowdRequest
    }

    private fun dataSource(): List<DataObject>? {
        val data = ArrayList<DataObject>()

        c = pre?.getStringValue(Constants.CYX_COIN_BALANCE)
        b = pre?.getStringValue(Constants.BITCOIN_BALANCE)
        e = pre?.getStringValue(Constants.ETHERCOIN_BALANCE)

        val s1 = pre?.getStringValue(Constants.CYX_COIN_BALANCE)
        val s2 = pre?.getStringValue(Constants.BITCOIN_BALANCE)

        data.add(DataObject(R.drawable.ic_eth_dashboard, R.drawable.yellow_shape,
                e.toString().plus(" " + "ETH"), "Wallet ETH Balance"))

        if (s1.equals("0E-8")) {
            c = "0"
            data.add(DataObject(R.drawable.img_cypher_dashboard, R.drawable.button_shape_orange,
                    c.toString().plus(" " + "CYX"), "Wallet CYX Balance"))
        } else {
            c = s1
            data.add(DataObject(R.drawable.img_cypher_dashboard, R.drawable.button_shape_orange,
                    c.toString().plus(" " + "CYX"), "Wallet CYX Balance"))
        }

        if (s2.equals("0E-8")) {
            b = "0"
            data.add(DataObject(R.drawable.ic_bitcoin, R.drawable.purple_shape,
                    b.toString().plus(" " + "BTC"), "Wallet BTC Balance"))
        } else {
            b = s2
            data.add(DataObject(R.drawable.ic_bitcoin, R.drawable.purple_shape,
                    b.toString().plus(" " + "BTC"), "Wallet BTC Balance"))
        }

        return data
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).toolbar_title.text = resources.getString(R.string.buyMoreTokens)
    }

    override fun onDetach() {
        super.onDetach()
        (activity as AppCompatActivity).toolbar_title.text = resources.getString(R.string.dashboard)
    }
}
