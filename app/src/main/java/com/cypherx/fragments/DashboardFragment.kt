package com.cypherx.fragments


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import com.cypherx.R
import com.cypherx.adapter.CustomPageAdapterNew
import com.cypherx.database.Preferences
import com.cypherx.interfaces.ApiInterfaces
import com.cypherx.models.Cyphercoin.CypherCoinRequest
import com.cypherx.models.Cyphercoin.CypherCoinResponse
import com.cypherx.models.DataObject
import com.cypherx.models.bitcoinbalance.BitcoinBalanceRequest
import com.cypherx.models.bitcoinbalance.BitcoinBalanceResponse
import com.cypherx.models.bonus.BonusRequest
import com.cypherx.models.bonus.BonusResponse
import com.cypherx.models.etherCoinBalance.EtherCoinRequest
import com.cypherx.models.etherCoinBalance.EtherCoinResponse
import com.cypherx.models.startdate.DateRequest
import com.cypherx.models.startdate.DateResponse
import com.cypherx.models.startdate.DateResponseItem
import com.cypherx.retrofit.APIClient
import com.cypherx.utils.CommonUtils
import com.cypherx.utils.Constants
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import dmax.dialog.SpotsDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class DashboardFragment : Fragment() {
    private var mContext: Context? = null
    private var dialog: SpotsDialog? = null
    var viewPager: ViewPager? = null
    private var mCustomPagerAdapter: CustomPageAdapterNew? = null
    private var pre: Preferences? = null
    private var btc: String? = null
    private var ether: String? = null
    private var bonusOne: String? = null
    private var bonusTwo: String? = null
    private var bonusThree: String? = null
    private var txtBonusOne: TextView? = null
    private var txtBonusTwo: TextView? = null
    private var txtBonusThree: TextView? = null
    private var txt_start: TextView? = null
    private var txt_end: TextView? = null
    private var txtPhase: TextView? = null
    private var txt_srtEnd: TextView? = null
    private var txtPhaseDas: TextView? = null
    private var b: String? = null
    private var c: String? = null
    private var e: String? = null
    private var strStartDate: String? = null
    private var strEndDate: String? = null
    private var datetime: String? = null
    var startandend: List<DateResponseItem>? = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_dashboard, container, false)
        initViews(v)
        return v
    }

    @SuppressLint("ResourceType")
    private fun initViews(v: View) {

        mContext = activity
        pre = Preferences()
        dialog = SpotsDialog(activity, resources.getString(R.string.message))
        dialog!!.setCancelable(false)

        btc = pre?.getStringValue("btcWall")
        ether = pre?.getStringValue("ethWall")


        if (CommonUtils.isNetworkAvailable(activity)) {
            bitCoinBalanceApi()
            etherBalanceApi()
            cypherBalanceApi()
            bonusApi()
            dateApi()
        } else {
            Toast.makeText(activity, R.string.network_check, Toast.LENGTH_SHORT).show()
        }


        viewPager = v.findViewById(R.id.activity_main_view_pager)
        txtBonusOne = v.findViewById(R.id.txtCoinPer) as TextView
        txtBonusTwo = v.findViewById(R.id.txtCoin) as TextView
        txtBonusThree = v.findViewById(R.id.txtCoinLess) as TextView
        txt_srtEnd = v.findViewById(R.id.txt_srtEnd) as TextView
        txtPhaseDas = v.findViewById(R.id.txtPhaseDas) as TextView
        val et_address = v.findViewById(R.id.edt_address) as EditText
        val et_wallet = v.findViewById(R.id.edt_wallet) as EditText
        val img_qr = v.findViewById(R.id.img_qr) as ImageView
        val imgCopybtc = v.findViewById(R.id.imgCopybtc) as ImageView
        val img_copy = v.findViewById(R.id.img_copy) as ImageView
        val img_qr_ethernet = v.findViewById(R.id.img_qr_ethernet) as ImageView
        txt_start = v.findViewById(R.id.txt_start) as TextView
        txt_end = v.findViewById(R.id.txt_end) as TextView
        txtPhase = v.findViewById(R.id.txtPhase) as TextView


        val getData = dataSource()
        mCustomPagerAdapter = CustomPageAdapterNew(mContext, getData)
        viewPager!!.pageMargin = 80
        viewPager!!.offscreenPageLimit = 30
        viewPager!!.adapter = mCustomPagerAdapter

        et_address.setText(btc)
        et_wallet.setText(ether)

        img_qr.setOnClickListener(clickListener)
        imgCopybtc.setOnClickListener(clickListener)
        img_copy.setOnClickListener(clickListener)
        img_qr_ethernet.setOnClickListener(clickListener)
    }

    private fun dateApi() {
        startandend = ArrayList()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiDate(buildDateRequest()!!)
        call.enqueue(object : Callback<DateResponse> {
            override fun onResponse(call: Call<DateResponse>?, response: Response<DateResponse>?) {
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            startandend = ArrayList()
                            startandend = response.body()!!.startEndDate
                            dateList(startandend)
                        } else {
                            Toast.makeText(activity, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }


            override fun onFailure(call: Call<DateResponse>?, t: Throwable?) {
                Toast.makeText(mContext, t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }


    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun dateList(datelist: List<DateResponseItem>?) {

        var p: String? = ""
        var s: String? = ""
        var d: String? = ""
        val c = Calendar.getInstance()
        val currentMonthFormat = SimpleDateFormat("MMM", Locale.US)
        datetime = currentMonthFormat.format(c.time)
        Log.d("myData", datetime)

        if (datelist != null && datelist.isNotEmpty()) {
            for (i in 0 until datelist.size) {
                s = datelist[i].startDate
                d = datelist[i].endDate
                p = datelist[i].phase

                Log.d("response date--1", s)
                Log.d("response date--2", d)

                val spf1 = SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa")
                val fromDate = spf1.parse(s)
                val toDate = spf1.parse(d)

                if (System.currentTimeMillis() > fromDate.time && System.currentTimeMillis() < toDate.time) {

                    val one = spf1.parse(s)
                    val two = spf1.parse(d)
                    val spf = SimpleDateFormat("MMM dd, yyyy")
                    val date1 = spf.format(one)
                    val date2 = spf.format(two)
                    txt_start!!.text = date1
                    txt_end!!.text = date2
                    txtPhase!!.text = p
                    txtPhaseDas!!.text = p
                    Log.d("phase***", p)

                    txt_srtEnd!!.text = "$s till $d"

                }
            }
        }
    }

    private fun buildDateRequest(): DateRequest? {
        val buildDateRequest = DateRequest()
        return buildDateRequest
    }


    private fun bonusApi() {
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiBonus(this.buildBonusRequest()!!)
        call.enqueue(object : Callback<BonusResponse> {
            override fun onFailure(call: Call<BonusResponse>?, t: Throwable?) {
                Toast.makeText(mContext, t!!.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<BonusResponse>?, response: Response<BonusResponse>?) {

                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            bonusOne = response.body()!!.viewBonus!!.bonusOne
                            bonusTwo = response.body()!!.viewBonus!!.bonusTwo
                            bonusThree = response.body()!!.viewBonus!!.bonusThree
                            setBonus(bonusOne, bonusTwo, bonusThree)
                        } else {
                            Toast.makeText(mContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        })
    }

    private fun setBonus(bonusOne: String?, bonusTwo: String?, bonusThree: String?) {

        txtBonusOne?.text = bonusThree.plus(" %")
        txtBonusTwo?.text = bonusTwo.plus(" %")
        txtBonusThree?.text = bonusOne.plus(" %")
    }


    private fun buildBonusRequest(): BonusRequest? {

        val buildBonusRequest = BonusRequest()
        buildBonusRequest.sessionId = pre?.getStringValue(Constants.SESSION)
        return buildBonusRequest
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.img_qr -> {
                qrDialog()
            }
            R.id.imgCopybtc -> {
                copyBtcAddress()
            }
            R.id.img_copy -> {
                copyEtherAddress()
            }

            R.id.img_qr_ethernet -> {
                qrEtherDialog()
            }
        }
    }

    private fun qrDialog() {
        val dialogView = LayoutInflater.from(mContext).inflate(R.layout.popup_qrcode, null)
        val qrCode = dialogView.findViewById(R.id.img_qr) as ImageView

        val builder = AlertDialog.Builder(mContext).setView(dialogView)
        val dialog = builder.show()
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(btc, BarcodeFormat.QR_CODE, 600, 600)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            qrCode.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace()
        }

        dialog.setCanceledOnTouchOutside(true)
    }

    private fun qrEtherDialog() {
        val dialogView = LayoutInflater.from(mContext).inflate(R.layout.popup_ether_qrcode, null)
        val qrCodeEther = dialogView.findViewById(R.id.img_qr_ethernet) as ImageView

        val builder = AlertDialog.Builder(mContext).setView(dialogView)
        val dialog = builder.show()
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(ether, BarcodeFormat.QR_CODE, 400, 400)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            qrCodeEther.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace()
        }

        dialog.setCanceledOnTouchOutside(true)

    }

    private fun copyEtherAddress() {

        if (ether!!.isNotEmpty()) {
            val clipboard = mContext!!.getSystemService(Context.CLIPBOARD_SERVICE) as android.text.ClipboardManager
            clipboard.text = ether
            Toast.makeText(mContext, R.string.ether_copy, Toast.LENGTH_SHORT).show()

        } else {
            val clipboard = mContext!!.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clip = android.content.ClipData.newPlainText("Clip", ether)
            Toast.makeText(mContext, R.string.text_copied, Toast.LENGTH_SHORT).show()
            clipboard.primaryClip = clip
        }
    }

    private fun copyBtcAddress() {

        if (btc!!.isNotEmpty()) {
            val clipboard = mContext!!.getSystemService(Context.CLIPBOARD_SERVICE) as android.text.ClipboardManager
            clipboard.text = btc
            Toast.makeText(mContext, R.string.btc_copy, Toast.LENGTH_SHORT).show()

        } else {
            val clipboard = mContext!!.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clip = android.content.ClipData.newPlainText("Clip", btc)
            Toast.makeText(mContext, R.string.text_copied, Toast.LENGTH_SHORT).show()
            clipboard.primaryClip = clip
        }

    }

    private fun cypherBalanceApi() {
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiCypherBalance(this.buildCypherRequest()!!)
        call.enqueue(object : Callback<CypherCoinResponse> {

            override fun onFailure(call: Call<CypherCoinResponse>?, t: Throwable?) {
                Toast.makeText(mContext, t!!.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<CypherCoinResponse>?,
                                    response: Response<CypherCoinResponse>?) {
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            val cyxBalance = response.body()!!.tokenBalanceInfo!!.tokBalance
                            pre?.setValue(Constants.CYX_COIN_BALANCE, "" + cyxBalance)

                            val getData = dataSource()
                            mCustomPagerAdapter = CustomPageAdapterNew(mContext, getData)
                            viewPager!!.adapter = mCustomPagerAdapter

                        } else {
                            //Toast.makeText(mContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        // Toast.makeText(mContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun buildCypherRequest(): CypherCoinRequest? {
        val buildCypherRequest = CypherCoinRequest()
        buildCypherRequest.sessionId = pre?.getStringValue(Constants.SESSION)
        return buildCypherRequest
    }

    private fun bitCoinBalanceApi() {
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiBitCoinBalance(this.buildBitcoinRequest()!!)
        call.enqueue(object : Callback<BitcoinBalanceResponse> {
            override fun onFailure(call: Call<BitcoinBalanceResponse>?, t: Throwable?) {
                Toast.makeText(mContext, t!!.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<BitcoinBalanceResponse>?,
                                    response: Response<BitcoinBalanceResponse>?) {
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            val bitBalance = response.body()!!.bitcoinBalanceInfo!!.bitcoinBalance.toString()
                            pre?.setValue(Constants.BITCOIN_BALANCE, "" + bitBalance)

                            val getData = dataSource()
                            mCustomPagerAdapter = CustomPageAdapterNew(mContext, getData)
                            viewPager!!.adapter = mCustomPagerAdapter

                        } else {
                            // Toast.makeText(mContext, "" + response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        })
    }

    private fun buildBitcoinRequest(): BitcoinBalanceRequest? {

        val buildBitcoinRequest = BitcoinBalanceRequest()
        buildBitcoinRequest.sessionId = pre?.getStringValue(Constants.SESSION)
        return buildBitcoinRequest
    }

    fun etherBalanceApi() {
        dialog?.show()
        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiEtherBalace(this.buildEtherBalance()!!)
        call.enqueue(object : Callback<EtherCoinResponse> {

            override fun onResponse(call: Call<EtherCoinResponse>?,
                                    response: Response<EtherCoinResponse>?) {
                dialog!!.dismiss()
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            val etherBalance = response.body()!!.etherBalanceInfo!!.etherBalance
                            pre?.setValue(Constants.ETHERCOIN_BALANCE, "" + etherBalance)

                            val getData = dataSource()
                            mCustomPagerAdapter = CustomPageAdapterNew(mContext, getData)
                            viewPager!!.adapter = mCustomPagerAdapter
                        } else {
                            // Toast.makeText(mContext, response.body()!!.message, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        //  Toast.makeText(mContext, response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<EtherCoinResponse>?, t: Throwable?) {
                dialog!!.dismiss()
                Toast.makeText(mContext, t!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun buildEtherBalance(): EtherCoinRequest? {
        val buildEtherBalance = EtherCoinRequest()
        buildEtherBalance.sessionId = pre?.getStringValue(Constants.SESSION)
        return buildEtherBalance
    }

    private fun dataSource(): List<DataObject> {
        val data = ArrayList<DataObject>()

        c = pre?.getStringValue(Constants.CYX_COIN_BALANCE)
        b = pre?.getStringValue(Constants.BITCOIN_BALANCE)
        e = pre?.getStringValue(Constants.ETHERCOIN_BALANCE)

        val s1 = pre?.getStringValue(Constants.CYX_COIN_BALANCE)
        val s2 = pre?.getStringValue(Constants.BITCOIN_BALANCE)

        data.add(DataObject(R.drawable.ic_eth_dashboard, R.drawable.yellow_shape,
                e.toString().plus(" " + "ETH"), "Wallet ETH Balance"))


        if (s1.equals("0E-8")) {
            c = "0"
            data.add(DataObject(R.drawable.img_cypher_dashboard, R.drawable.button_shape_orange,
                    c.toString().plus(" " + "CYX"), "Wallet CYX Balance"))
        } else {
            c = s1
            data.add(DataObject(R.drawable.img_cypher_dashboard, R.drawable.button_shape_orange,
                    c.toString().plus(" " + "CYX"), "Wallet CYX Balance"))
        }

        if (s2.equals("0E-8")) {
            b = "0"
            data.add(DataObject(R.drawable.ic_bitcoin, R.drawable.purple_shape,
                    b.toString().plus(" " + "BTC"), "Wallet BTC Balance"))
        } else {
            b = s2
            data.add(DataObject(R.drawable.ic_bitcoin, R.drawable.purple_shape,
                    b.toString().plus(" " + "BTC"), "Wallet BTC Balance"))
        }


        return data
    }
}
