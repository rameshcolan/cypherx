package com.cypherx.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.cypherx.R
import com.cypherx.adapter.HistoryAdapter
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import com.cypherx.database.Preferences
import com.cypherx.interfaces.ApiInterfaces
import com.cypherx.models.history.HistoryRequest
import com.cypherx.models.history.HistoryResponse
import com.cypherx.retrofit.APIClient
import com.cypherx.utils.CommonUtils
import com.cypherx.utils.Constants
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.app_bar_dashboard.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HistoryFragment : Fragment() {

    private var dialog: SpotsDialog? = null
    var historyRecycler: RecyclerView? = null
    var history = ArrayList<HistoryResponse.TransactionHistory>()
    var mContext: Context? = null
    var noData: TextView? = null
    var pre: Preferences? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_history, container, false)
        initViews(view)
        return view
    }

    @SuppressLint("ResourceType")
    private fun initViews(view: View) {
        pre = Preferences()
        history = ArrayList()
        dialog = SpotsDialog(activity, resources.getString(R.string.message))
        dialog!!.setCancelable(false)

        historyRecycler = view.findViewById(R.id.rl_history) as RecyclerView
        noData = view.findViewById(R.id.noData) as TextView
        historyRecycler!!.layoutManager = LinearLayoutManager(mContext)

        if (CommonUtils.isNetworkAvailable(activity)) {
            HistoryApi()
        } else {
            Toast.makeText(activity, R.string.network_check, Toast.LENGTH_SHORT).show()
        }


    }

    private fun HistoryApi() {
        dialog!!.show()

        val apiInterfaces = APIClient.getClient().create(ApiInterfaces::class.java)
        val call = apiInterfaces.apiHistory(buildSignUp()!!)
        call.enqueue(object : Callback<HistoryResponse> {
            override fun onResponse(call: Call<HistoryResponse>?, response: Response<HistoryResponse>?) {
                dialog!!.dismiss()
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body()!!.status == Constants.SERVICE_SUCCESS) {
                            history = ArrayList()
                            if (response.body()!!.transactionHistory != null && response.body()!!.transactionHistory.size > 0) {
                                noData!!.visibility = View.GONE
                                historyRecycler!!.visibility = View.VISIBLE
                                history.clear()
                                val history = response.body()!!.transactionHistory
                                historyRecycler!!.adapter = HistoryAdapter(mContext, history)
                            } else {
                                noData!!.visibility = View.VISIBLE
                                historyRecycler!!.visibility = View.GONE
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<HistoryResponse>?, t: Throwable?) {
                dialog!!.dismiss()
                CommonUtils.showToast(activity!!, "" + t?.message)
            }
        })
    }

    private fun buildSignUp(): HistoryRequest? {
        var history = HistoryRequest()
        history.sessionId = pre?.getStringValue(Constants.SESSION)
        return history
    }


    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).toolbar_title.text = resources.getString(R.string.history)
    }

    override fun onDetach() {
        super.onDetach()
        (activity as AppCompatActivity).toolbar_title.text = resources.getString(R.string.dashboard)
    }
}
