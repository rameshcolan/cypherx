package com.cypherx.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface

object Constants {

    const val SERVICE_LOGIN = "login"
    const val SERVICE_SIGNUP = "signup"
    const val SERVICE_HISTORY = "token/recentTransactionHistory"
    const val SERVICE_FORGOT = "forgot/password"
    const val SERVICE_LOGOUT = "logout"
    const val SERVICE_BITCOIN_BALANCE = "bitcoin/balance"
    const val SERVICE_CONTRIBUTE_CROWDSALE = "token/contributeInCrowdsale"
    const val SERVICE_ETHER_BALANCE = "ether/balance"
    const val SERVICE_CYPHERX_BALANCE = "token/balance"
    const val SERVICE_SUCCESS = "Success"
    const val SERVICE_BONUS = "token/view/bonus"
    const val SERVICE_LIVE_CRYPTO_PRICE = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH&tsyms=USD"
    const val SERVICE_TOKEN = "today/token/rate"
    const val SERVICE_DATE = "start/date"
    const val SESSION = "sessionId"
    const val ROLEID = "roleId"
    const val CYX_COIN_BALANCE = "cyxCoinBalance"
    const val BITCOIN_BALANCE = "bitCoinBalance"
    const val ETHERCOIN_BALANCE = "etherCoinBalance"
    const val BTC = "btcWall"
    const val ETH = "ethWall"
    const val USER = "user"
    const val EMAIL = "email"
    const val SERVER_ERROR = "Server Error"

    const val IS_LOGGED_IN = true

    fun alertDialogShow(context: Context, message: String) {
        val alertDialog = AlertDialog.Builder(context).create()
        alertDialog.setMessage(message)
        alertDialog.setButton("OK", DialogInterface.OnClickListener { dialog, which -> alertDialog.dismiss() })
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()
    }

}
