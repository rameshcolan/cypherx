package com.cypherx.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.ConnectivityManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.util.Log
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by CIPL0349 on 12/15/2017.
 */
object CommonUtils {

    val Premission = arrayOf(Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.ACCESS_COARSE_LOCATION)

    private const val EMPTY = ""

    /**
     *
     * Joins the elements of the provided `Iterator` into
     * a single String containing the provided elements.
     *
     *
     * No delimiter is added before or after the list.
     * A `null` separator is the same as an empty String ("").
     *
     *
     * @param iterator  the `Iterator` of values to join together, may be null
     * @param separator  the separator character to use, null treated as ""
     * @return the joined String, `null` if null iterator input
     */
    fun join(iterator: Iterator<*>?, separator: String?): String? {

        // handle null, zero and one elements before building a buffer
        if (iterator == null) {
            return null
        }
        if (!iterator.hasNext()) {
            return EMPTY
        }
        val first = iterator.next()
        if (!iterator.hasNext()) {
            return toString(first)
        }

        // two or more elements
        val buf = StringBuilder(256) // Java default is 16, probably too small
        if (first != null) {
            buf.append(first)
        }

        while (iterator.hasNext()) {
            if (separator != null) {
                buf.append(separator)
            }
            val obj = iterator.next()
            if (obj != null) {
                buf.append(obj)
            }
        }
        return buf.toString()
    }

    private fun toString(obj: Any?): String {
        return obj?.toString() ?: ""
    }

    fun dp2px(context: Context, dipValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dipValue * scale + 0.5f).toInt()
    }

    fun isNetworkAvailable(context: Context?): Boolean {
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }

    fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    Log.e("permission", "" + permission)
                    return false
                }
            }
        }
        return true
    }

    fun getDateTime(): String {
        val timeStamp = SimpleDateFormat("yyyy-MM-dd_HH_mm_ss").format(Calendar.getInstance().time)
        println(timeStamp)
        return timeStamp
    }

    fun getDateTimeFormData(): String {
        val timeStamp = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().time)
        println(timeStamp)
        return timeStamp
    }

//    fun getDay(): String{
//        val dayOfWeek = date.get(Calendar.DAY_OF_WEEK)
//        val weekday = DateFormatSymbols().getShortWeekdays()[dayOfWeek]
//    }

    fun getResizedBitmap(bm: Bitmap?, newWidth: Int, newHeight: Int): Bitmap {
        val width = bm?.width
        val height = bm?.height
        val scaleWidth = newWidth.toFloat() / width!!
        val scaleHeight = newHeight.toFloat() / height!!
        // CREATE A MATRIX FOR THE MANIPULATION
        val matrix = Matrix()
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight)

        // "RECREATE" THE NEW BITMAP
        val resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false)
        bm.recycle()
        return resizedBitmap
    }

//    fun showHelpDialog(mContext: Context) {
//        val dialog = Dialog(mContext)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setCancelable(false)
////        dialog.setContentView(layout.dialog_fragment_timer)
//        dialog.window.setBackgroundDrawableResource(android.R.color.transparent)
//        val tvCancel = dialog.findViewById(id.lly_dailog) as LinearLayout
//        val btnbuy = dialog.findViewById(id.btnBuy) as Button
//
//        btnbuy.setOnClickListener {
//
//        }
//        tvCancel.setOnClickListener {
//            dialog.dismiss()
//        }
//        dialog.show()
//    }


//
//    fun showDialog(activity: Context) {
//        val dialog = Dialog(activity)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setCancelable(false)
//        dialog.setContentView(layout.time_picker_dialog)
//        dialog.window.setBackgroundDrawableResource(android.R.color.transparent)
//
//    }


    fun humanReadableCount(bytes: Double, si: Boolean): String {
        val unit = if (si) 1000 else 1024
        if (bytes < unit) return bytes.toString() + " B"
        val exp = (Math.log(bytes) / Math.log(unit.toDouble())).toInt()
        val pre = (if (si) "kMGTPE" else "KMGTPE")[exp - 1] + if (si) "" else "i"
        return String.format("%.1f %sB", bytes / Math.pow(unit.toDouble(), exp.toDouble()), pre)
    }

    fun showToast(activity: Context,msg:String){
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }
}