package com.cypherx.models.startdate

import com.google.gson.annotations.SerializedName

class DateResponse {
    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("startEndDate")
    var startEndDate: List<DateResponseItem>? = null

}
