package com.cypherx.models.startdate

import com.google.gson.annotations.SerializedName

class DateResponseItem {

    @SerializedName("transferStatus")
    var transferStatus: Boolean? = null
    @SerializedName("StartDate")
    var startDate: String? = null
    @SerializedName("EndDate")
    var endDate: String? = null
    @SerializedName("Phase")
    var phase: String? = null

}
