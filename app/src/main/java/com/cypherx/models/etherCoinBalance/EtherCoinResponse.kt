package com.cypherx.models.etherCoinBalance

import com.google.gson.annotations.SerializedName

class EtherCoinResponse {
    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("etherBalanceInfo")
    var etherBalanceInfo: EtherCoinResponseItem? = null

}
