package com.cypherx.models.etherCoinBalance

import com.google.gson.annotations.SerializedName

class EtherCoinResponseItem {

    @SerializedName("sessionId")
    var sessionId: String? = null
    @SerializedName("centralAdmin")
    var centralAdmin: String? = null
    @SerializedName("etherBalance")
    var etherBalance: String? = null

}
