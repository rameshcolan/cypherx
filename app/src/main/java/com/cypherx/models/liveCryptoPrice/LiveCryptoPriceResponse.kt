package com.cypherx.models.liveCryptoPrice

import com.google.gson.annotations.SerializedName

class LiveCryptoPriceResponse {

    @SerializedName("BTC")
    var btc: BTC? = null

    @SerializedName("ETH")
    var eth: ETH? = null

}
