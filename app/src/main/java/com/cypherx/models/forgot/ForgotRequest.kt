package com.cypherx.models.forgot

import com.google.gson.annotations.SerializedName

class ForgotRequest {

    @SerializedName("emailId")
    var emailId: String? = null

}