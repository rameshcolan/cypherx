package com.cypherx.models.forgot

import com.google.gson.annotations.SerializedName

class ForgotResponse {

    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null

}
