package com.cypherx.models.history

import com.google.gson.annotations.SerializedName

class HistoryRequest {

    @SerializedName("sessionId")
    var sessionId: String? = null

}
