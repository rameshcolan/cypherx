package com.cypherx.models.history

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose



class HistoryResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("transactionHistory")
    var transactionHistory: ArrayList<TransactionHistory> = ArrayList()

    class TransactionHistory {
        @SerializedName("fromAddress")
        var fromAddress: String? = null

        @SerializedName("toAddress")
        var toAddress: String? = null

        @SerializedName("createdDate")
        var createdDate: String? = null

        @SerializedName("transferedAmount")
        var transferedAmount: String? = null

        @SerializedName("transferStatus")
        var transferStatus: Boolean? = null

        @SerializedName("transferMode")
        var transferMode: String? = null
    }
}
