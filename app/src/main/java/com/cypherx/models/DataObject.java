package com.cypherx.models;

public class DataObject {

    private int imageName;
    private int imageId;
    private String imgCoinBalance;
    private String txtCoinText;

    public String getTxtCoinText() {
        return txtCoinText;
    }

    public void setTxtCoinText(String txtCoinText) {
        this.txtCoinText = txtCoinText;
    }

    public int getImageName() {
        return imageName;
    }

    public String getImgCoinBalance() {
        return imgCoinBalance;
    }

    public void setImgCoinBalance(String imgCoinBalance) {
        this.imgCoinBalance = imgCoinBalance;
    }


    public DataObject(int imageId, int imageName , String imgCoinBalance,String txtCoinText) {
        this.imageId = imageId;
        this.imageName = imageName;
        this.imgCoinBalance = imgCoinBalance;
        this.txtCoinText = txtCoinText;
    }

    public int getImageId() {
        return imageId;
    }
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
