package com.cypherx.models.signup

import com.google.gson.annotations.SerializedName

class SignUpRequest {
    @SerializedName("bitcoinWalletPassword")
    var bitcoinWalletPassword: String? = null
    @SerializedName("confirmPassword")
    var confirmPassword: String? = null
    @SerializedName("emailId")
    var emailId: String? = null
    @SerializedName("etherWalletPassword")
    var etherWalletPassword: String? = null
    @SerializedName("password")
    var password: String? = null
    @SerializedName("referenceId")
    var referenceId: String? = null
    @SerializedName("userName")
    var userName: String? = null
}
