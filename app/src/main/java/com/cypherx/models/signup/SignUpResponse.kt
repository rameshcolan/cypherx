package com.cypherx.models.signup

import com.google.gson.annotations.SerializedName

class SignUpResponse {

    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null

}
