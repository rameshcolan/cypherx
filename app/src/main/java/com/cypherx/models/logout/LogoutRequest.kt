package com.cypherx.models.logout

import com.google.gson.annotations.SerializedName

class LogoutRequest {

    @SerializedName("sessionId")
    var sessionId: String? = null

}