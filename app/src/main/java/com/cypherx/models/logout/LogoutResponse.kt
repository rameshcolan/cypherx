package com.cypherx.models.logout

import com.google.gson.annotations.SerializedName

class LogoutResponse {
    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null

}