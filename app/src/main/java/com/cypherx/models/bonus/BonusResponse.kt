package com.cypherx.models.bonus

import com.google.gson.annotations.SerializedName

class BonusResponse {
    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("viewBonus")
    var viewBonus: BonusResponseItem? = null

}
