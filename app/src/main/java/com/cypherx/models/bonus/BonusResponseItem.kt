package com.cypherx.models.bonus

import com.google.gson.annotations.SerializedName

class BonusResponseItem {
    @SerializedName("sessionId")
    var sessionId: String? = null
    @SerializedName("transferStatus")
    var transferStatus: Boolean? = null
    @SerializedName("bonusOne")
    var bonusOne: String? = null
    @SerializedName("bonusTwo")
    var bonusTwo: String? = null
    @SerializedName("bonusThree")
    var bonusThree: String? = null

}
