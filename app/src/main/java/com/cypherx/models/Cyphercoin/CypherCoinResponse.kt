package com.cypherx.models.Cyphercoin

import com.google.gson.annotations.SerializedName

class CypherCoinResponse {
    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("TokenBalanceInfo")
    var tokenBalanceInfo: CypherCoinResponseItem? = null

}
