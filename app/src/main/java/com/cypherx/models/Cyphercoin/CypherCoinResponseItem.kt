package com.cypherx.models.Cyphercoin

import com.google.gson.annotations.SerializedName

class CypherCoinResponseItem {
    @SerializedName("sessionId")
    var sessionId: String? = null
    @SerializedName("transferStatus")
    var transferStatus: Boolean? = null
    @SerializedName("confirmedTokens")
    var confirmedTokens: String? = null
    @SerializedName("pendingTokens")
    var pendingTokens: String? = null
    @SerializedName("soldTokens")
    var soldTokens: String? = null
    @SerializedName("tokBalance")
    var tokBalance: String? = null

}
