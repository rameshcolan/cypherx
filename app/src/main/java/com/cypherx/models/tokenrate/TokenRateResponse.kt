package com.cypherx.models.tokenrate

import com.google.gson.annotations.SerializedName

class TokenRateResponse {
    @SerializedName("status")
    var status: String? = null
    @SerializedName("tokenRate")
    var tokenRate: Float? = null

}
