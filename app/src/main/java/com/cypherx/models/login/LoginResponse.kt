package com.cypherx.models.login

import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("loginInfo")
    var loginInfo: LoginResponseItem? = null

}
