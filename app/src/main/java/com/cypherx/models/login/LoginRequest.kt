package com.cypherx.models.login

import com.google.gson.annotations.SerializedName

class LoginRequest {


    @SerializedName("emailId")
    var emailId: String? = null
    @SerializedName("password")
    var password: String? = null

}