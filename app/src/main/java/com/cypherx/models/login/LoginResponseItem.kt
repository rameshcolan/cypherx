package com.cypherx.models.login

import com.google.gson.annotations.SerializedName

class LoginResponseItem {
    @SerializedName("userName")
    var userName: String? = null
    @SerializedName("emailId")
    var emailId: String? = null
    @SerializedName("etherWalletAddress")
    var etherWalletAddress: String? = null
    @SerializedName("status")
    var status: String? = null
    @SerializedName("roleId")
    var roleId: Int? = null
    @SerializedName("sessionId")
    var sessionId: String? = null
    @SerializedName("bitcoinWalletReceivingAddress")
    var bitcoinWalletReceivingAddress: String? = null
    @SerializedName("QRCode")
    var qrCode: String? = null
    @SerializedName("lastLogin")
    var lastLogin: String? = null

}
