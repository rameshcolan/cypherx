package com.cypherx.models.bitcoinbalance

import com.google.gson.annotations.SerializedName

class BitcoinBalanceResponse {
    @SerializedName("status")
    var status: String? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("bitcoinBalanceInfo")
    var bitcoinBalanceInfo: BitcoinBalanceResponseItem? = null

}
