package com.cypherx.models.bitcoinbalance

import com.google.gson.annotations.SerializedName

class BitcoinBalanceResponseItem {


    @SerializedName("sessionId")
    var sessionId: String? = null
    @SerializedName("bitcoinBalance")
    var bitcoinBalance: String? = null

}

