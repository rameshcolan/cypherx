package com.cypherx.models.contributeCrowdSale

import com.google.gson.annotations.SerializedName

class EtherBuyRequest {

    @SerializedName("sessionId")
    var sessionId: String? = null
    @SerializedName("selectTransactionType")
    var selectTransactionType: Int? = null
    @SerializedName("etherWalletPassword")
    var etherWalletPassword: String? = null
    @SerializedName("requestTokens")
    var requestTokens: Double? = null

    @SerializedName("bitcoinWalletPassword")
    var bitcoinWalletPassword: String? = null

}
