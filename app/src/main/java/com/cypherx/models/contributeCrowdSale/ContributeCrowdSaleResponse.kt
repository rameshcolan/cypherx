package com.cypherx.models.contributeCrowdSale

import com.google.gson.annotations.SerializedName

class ContributeCrowdSaleResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("message")
    var message: String? = null

}
